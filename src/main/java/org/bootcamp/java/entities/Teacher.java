package org.bootcamp.java.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//Lombok
@NoArgsConstructor
@Data

//JPA
@Entity
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String firstName;
    String lastName;
    Integer age;

   public Teacher(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = 18;
    }


}
