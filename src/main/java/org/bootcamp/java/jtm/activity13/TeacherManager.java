package org.bootcamp.java.jtm.activity13;

import java.util.Optional;

import org.bootcamp.java.entities.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeacherManager {

	@Autowired
	TeacherRepository teacherRepository;


	public Teacher findTeacher(int id) {
		Optional<Teacher> teacher = teacherRepository.findById(id);
		return teacher.isPresent() ? teacher.get() : new Teacher();
	}


	public boolean insertTeacher(String firstName, String lastName) {
		return insertTeacher(new Teacher(firstName,lastName));
	}

	public boolean insertTeacher(Teacher teacher) {
		teacherRepository.save(teacher);
		return true;
	}


	public boolean updateTeacher(Teacher teacher) {
		if(teacherRepository.existsById(teacher.getId())) {
			teacherRepository.save(teacher);
			return true;
		}
		return false;
	}


	public boolean deleteTeacher(int id) {
		if(teacherRepository.existsById(id)) {
			teacherRepository.deleteById(id);
			return true;
		}
		return false;
	}

}
