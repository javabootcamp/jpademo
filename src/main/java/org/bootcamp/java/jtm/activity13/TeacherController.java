package org.bootcamp.java.jtm.activity13;

import org.bootcamp.java.entities.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TeacherController {

    @Autowired
    private TeacherRepository repo;

    @GetMapping("/teacher")
    @ResponseBody
    public Teacher insertTeacher(@RequestParam(name = "firstName") String firstName,
                                 @RequestParam(name = "lastName") String lastName){
        return repo.save(new Teacher(firstName,lastName));
    }

    @GetMapping("/teachers")
    @ResponseBody
    public Iterable<Teacher> allTeachers(){
        return repo.findAll();
    }


    @GetMapping("/teachersOverAge")
    @ResponseBody
    public Iterable<Teacher> allTeachers(@RequestParam(name = "age") Integer age){
        return repo.findByAgeGreaterThan(age);
    }

    @GetMapping("/teachersOverAgeWithName")
    @ResponseBody
    public Iterable<Teacher> allTeachers(@RequestParam(name = "age") Integer age,
                                         @RequestParam(name = "name") String name){
        return repo.findByAgeGreaterThanAndFirstNameLike(age,name);
    }

}
