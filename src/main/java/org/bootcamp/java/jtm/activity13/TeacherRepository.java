package org.bootcamp.java.jtm.activity13;

import org.bootcamp.java.entities.Teacher;
import org.springframework.data.repository.CrudRepository;

public interface TeacherRepository extends CrudRepository<Teacher, Integer> {

    Iterable<Teacher> findByAgeGreaterThan(int age);

    Iterable<Teacher> findByAgeGreaterThanAndFirstNameLike(int age, String firstName);

}
